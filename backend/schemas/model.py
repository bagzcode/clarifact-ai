from pydantic import BaseModel

class Hoax(BaseModel):
    judul: str
    narasi: str

class Output(BaseModel):
    prediction: str
    percentage: float
